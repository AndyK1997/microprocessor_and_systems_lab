// L�sung zur Aufgabe Termin4
// Aufgabe 1
//****************************
// Zeiger auf Peripherie
// Messen der Periodendauer einer angelegten Frequenz
// 
// von: Manfred Pester
// vom: 06. August 2003

#include "../h/pio.h"
#include "../h/tc.h"
#include "../h/pmc.h"

// f�r die Initialisierung des Z�hler TC4
#define TC4_INIT  TC_CLKS_MCK2 | TC_LDBSTOP | TC_CAPT | TC_LDRA_RISING_EDGE | TC_LDRB_RISING_EDGE
//                  0x0             0x40        0x0         0x10000               0x40000

int main(void)
{
	volatile int	captureRA1;
	volatile int	captureRB1;
	volatile int	capturediff1;
	volatile float Periodendauer1;
	
	StructPMC* pmcbase = PMC_BASE;
	StructPIO* piobaseA = PIOA_BASE;
    StructPIO* piobaseB = PIOB_BASE;
	StructTC* tcbase4 = TCB4_BASE;
	StructTC* tcbase5 = TCB5_BASE;
	
	pmcbase->PMC_PCER = 0x06400;	// Clock fuer PIOA, PIOB und Timer4, einschalten
	
// Periodendauer einer der von der Waage gelieferten Frequenz messen
// Die Waage liefert ca. 16kHz entspricht einer Periodendauer von 62,5�s
// durch den Vorteiler von 32 ergibt sich an TIOA4 eine Periodendauer von ca. 2ms (62,5�s x 32 = 2000�s / 1000 = 2ms)
// ..


	piobaseA->PIO_PDR	=	0x090;		//write only, Timer4 und Timer5 aktivieren
	tcbase4->TC_CCR		=	TC_CLKDIS;  //0x2 -> CounterClockDisableCommand = 1 -> disables the clock
	tcbase4->TC_CMR		=	TC4_INIT;   //ChannelModeRegister S.116; TC4_INIT oben als define
	tcbase4->TC_CCR		=	TC_CLKEN;   //0x1 -> CounterClockEnableCommand = 1 -> enables the clock, if TC_CLKDIS !=1
	tcbase4->TC_CCR		=	TC_SWTRG;   //0x4 -> Softw.TriggerCmd=1 -> softw.trigger is performed (counter reset & clock started)
	
	while(1) // soll dauerhaft laufen, nicht nur wenn Knopf3 gedr�ckt wird
	{
		tcbase4->TC_CCR	=	TC_SWTRG; // ControlRegister bekommt SoftwareTrigger (f�r Clock4)
		while (!( tcbase4->TC_SR & 0x40));		// Capture Register B wurde geladen Messung abgeschlossen
			captureRA1	= tcbase4->TC_RA;	//Timer Register A speichern
			captureRB1	= tcbase4->TC_RB;     // Timer Register B speichern
			capturediff1	= abs(captureRB1) - abs(captureRA1);    // differenz = Absolute,positive WertB - WertA
			Periodendauer1 = abs(capturediff1) / 12.5;	// Periodendauer = differenz/12,5us
			/*wir teilen die Differenz durch 12,5us weil: prozessortakt: 25mhz -> 25mhz/2 = 12,5*10^6 hz*/
    }
	
	return 0;
}
