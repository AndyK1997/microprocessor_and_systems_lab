// L�sung zur Aufgabe Termin4
// Aufgabe 4
//****************************
// Zeiger auf Peripherie
// Messen der aufgelegten Masse
// 
// von: Manfred Pester
// vom: 06. August 2003

#include "../h/pio.h"
#include "../h/tc.h"
#include "../h/pmc.h"

// f�r die Initialisierung des Z�hler TC4
#define TC4_INIT  TC_CLKS_MCK2 | TC_LDBSTOP | TC_CAPT | TC_LDRA_RISING_EDGE | TC_LDRB_RISING_EDGE
//                  0x0             0x40        0x0         0x10000               0x40000
#define TC5_INIT  TC_CLKS_MCK2 | TC_LDBSTOP | TC_CAPT | TC_LDRA_RISING_EDGE | TC_LDRB_RISING_EDGE

    StructTC* tcbase4 = TCB4_BASE;
	StructTC* tcbase5 = TCB5_BASE;
    
    volatile int	captureRA1;
	volatile int	captureRB1;
	volatile int	capturediff1;
    volatile int	captureRA2;
	volatile int	captureRB2;
	volatile int	capturediff2;
    
    volatile float Periodendauer1;
    volatile float Periodendauer2;
    
void PIO_Init(void)
{
    StructPIO* piobaseA = PIOA_BASE;
    StructPIO* piobaseB = PIOB_BASE;
    piobaseA->PIO_PDR	=	0x090;		//write only, Timer4 und Timer5 aktivieren
}

void Timer_Init(void)
{
    tcbase4->TC_CCR		=	TC_CLKDIS;  //0x2 -> CounterClockDisableCommand = 1 -> disables the clock
	tcbase4->TC_CMR		=	TC4_INIT;   //ChannelModeRegister S.116; TC4_INIT oben als define
	tcbase4->TC_CCR		=	TC_CLKEN;   //0x1 -> CounterClockEnableCommand = 1 -> enables the clock, if TC_CLKDIS !=1
	tcbase4->TC_CCR		=	TC_SWTRG;   //0x4 -> Softw.TriggerCmd=1 -> softw.trigger is performed (counter reset & clock started)
	
	tcbase5->TC_CCR = TC_CLKDIS;
    tcbase5->TC_CMR = TC5_INIT;
    tcbase5->TC_CCR = TC_CLKEN;
    tcbase5->TC_CCR = TC_SWTRG;
}

int MessungDerMasse(void)
{
    tcbase4->TC_CCR	=	TC_SWTRG; // ControlRegister bekommt SoftwareTrigger (f�r Clock4)
    tcbase5->TC_CCR =   TC_SWTRG;
        
    while (!(tcbase4->TC_SR & 0x40));		// Status Register mit 0x40 f�r LDRBS abgleichen 
                                            // Capture Register B wurde geladen Messung abgeschlossen
    captureRA1	= tcbase4->TC_RA;	//Timer Register A speichern
    captureRB1	= tcbase4->TC_RB;     // Timer Register B speichern
    capturediff1	= abs(captureRB1) - abs(captureRA1);    // differenz = Absolute,positive WertB - WertA
    Periodendauer1 = abs(capturediff1) / 12.5;	// Periodendauer = differenz/12,5us
    while (!(tcbase5->TC_SR & 0x40));
    captureRA2 = tcbase5->TC_RA;
    captureRB2 = tcbase5->TC_RB;
    capturediff2 = abs(captureRB2)-abs(captureRA2);
    Periodendauer2 = abs(capturediff2) / 12.5;
/*wir teilen die Differenz durch 12,5us weil: prozessortakt: 25mhz -> 25mhz/2 = 12,5*10^6 hz*/
    int Ergebnis = (2000*((Periodendauer1/Periodendauer2)-1)-0);
    return abs(Ergebnis);
}

int main(void)
{
    StructPMC* pmcbase = PMC_BASE;
    pmcbase->PMC_PCER = 0x06C00;	// Clock fuer PIOA, PIOB, Timer4, Timer5 einschalten
    
	volatile int Masse;
	
	PIO_Init();	
	Timer_Init();
	
	while(1)
	{
		Masse = MessungDerMasse();
	}
	
	return 0;
}
