	.file	"Termin5Aufgabe1.c"
	.section	.rodata
	.align	2
.LC0:
	.ascii	"Hallo\012\000"
	.align	2
.LC1:
	.ascii	"\012\000"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
	add	fp, sp, #4
	bl	inits
	ldr	r0, .L3
	bl	puts
	ldr	r0, .L3+4
	bl	puts
	mov	r0, #-2097152000
	mov	r0, r0, asr #20
	bl	convertInt
	mov	r3, #0
	mov	r0, r3
	ldmfd	sp!, {fp, pc}
.L4:
	.align	2
.L3:
	.word	.LC0
	.word	.LC1
	.size	main, .-main
	.ident	"GCC: (crosstool-NG 1.13.3 - arm-elf) 4.4.6"
