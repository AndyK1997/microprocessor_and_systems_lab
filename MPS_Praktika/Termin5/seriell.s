	.file	"seriell.c"
	.text
	.align	2
	.global	init_ser
	.type	init_ser, %function
init_ser:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #12
	mov	r3, #-1342177280
	mov	r3, r3, asr #14
	str	r3, [fp, #-12]
	mov	r3, #-1610612736
	mov	r3, r3, asr #15
	str	r3, [fp, #-8]
	mov	r3, #-2147483648
	mov	r3, r3, asr #13
	str	r3, [fp, #-4]
	ldr	r3, [fp, #-8]
	mov	r2, #4
	str	r2, [r3, #16]
	ldr	r3, [fp, #-12]
	mov	r2, #98304
	str	r2, [r3, #4]
	ldr	r3, [fp, #-4]
	mov	r2, #160
	str	r2, [r3, #0]
	ldr	r3, [fp, #-4]
	mov	r2, #41
	str	r2, [r3, #32]
	ldr	r3, [fp, #-4]
	mov	r2, #2240
	str	r2, [r3, #4]
	ldr	r3, [fp, #-4]
	mov	r2, #80
	str	r2, [r3, #0]
	mov	r3, #0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
	.size	init_ser, .-init_ser
	.align	2
	.global	putch
	.type	putch, %function
putch:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #8
	mov	r3, r0
	strb	r3, [fp, #-8]
	mov	r3, #-2147483648
	mov	r3, r3, asr #13
	str	r3, [fp, #-4]
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #20]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L4
	ldrb	r2, [fp, #-8]	@ zero_extendqisi2
	ldr	r3, [fp, #-4]
	str	r2, [r3, #28]
	b	.L5
.L4:
	mov	r3, #0
	strb	r3, [fp, #-8]
.L5:
	ldrb	r3, [fp, #-8]	@ zero_extendqisi2
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
	.size	putch, .-putch
	.align	2
	.global	getch
	.type	getch, %function
getch:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #8
	mov	r3, #-2147483648
	mov	r3, r3, asr #13
	str	r3, [fp, #-8]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #20]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L8
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #24]
	strb	r3, [fp, #-1]
	b	.L9
.L8:
	mov	r3, #0
	strb	r3, [fp, #-1]
.L9:
	ldrb	r3, [fp, #-1]	@ zero_extendqisi2
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
	.size	getch, .-getch
	.section	.rodata
	.align	2
.LC0:
	.ascii	"-214783648\000"
	.text
	.align	2
	.global	convertInt
	.type	convertInt, %function
convertInt:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #24
	str	r0, [fp, #-28]
	ldr	r3, [fp, #-28]
	cmp	r3, #-2147483648
	bne	.L12
	ldr	r0, .L22
	bl	puts
	b	.L20
.L12:
	mov	r3, #0
	strb	r3, [fp, #-13]
	mov	r3, #0
	str	r3, [fp, #-8]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bge	.L14
	ldr	r3, [fp, #-28]
	rsb	r3, r3, #0
	str	r3, [fp, #-28]
	mov	r3, #1
	str	r3, [fp, #-8]
.L14:
	mov	r3, #10
	str	r3, [fp, #-12]
	b	.L15
.L18:
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-28]
	ldr	r2, .L22+4
	smull	ip, r3, r2, r1
	mov	r2, r3, asr #2
	mov	r3, r1, asr #31
	rsb	r2, r3, r2
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	and	r3, r2, #255
	add	r3, r3, #48
	and	r2, r3, #255
	mvn	r3, #19
	sub	ip, fp, #4
	add	r1, ip, r0
	add	r3, r1, r3
	strb	r2, [r3, #0]
	ldr	r3, [fp, #-28]
	ldr	r1, .L22+4
	smull	r0, r2, r1, r3
	mov	r2, r2, asr #2
	mov	r3, r3, asr #31
	rsb	r3, r3, r2
	str	r3, [fp, #-28]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	beq	.L21
.L16:
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
.L15:
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bgt	.L18
	b	.L17
.L21:
	mov	r0, r0	@ nop
.L17:
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L19
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
	ldr	r2, [fp, #-12]
	mvn	r3, #19
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	mov	r2, #45
	strb	r2, [r3, #0]
.L19:
	ldr	r2, [fp, #-12]
	sub	r3, fp, #24
	add	r3, r3, r2
	mov	r0, r3
	bl	puts
.L20:
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L23:
	.align	2
.L22:
	.word	.LC0
	.word	1717986919
	.size	convertInt, .-convertInt
	.ident	"GCC: (crosstool-NG 1.13.3 - arm-elf) 4.4.6"
