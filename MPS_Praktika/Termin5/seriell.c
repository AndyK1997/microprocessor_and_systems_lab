/*----------------------------------------------------------------------------
@ File Name         : seriell.c
@ Object            : Grundfunktionen der seriellen Schnittstelle 
@	int init_ser(); char putch(char); char getch();
@
@ Autor 			: M.Pester
@ Datum				: 04.12.2007
@ 
@----------------------------------------------------------------------------*/

#include "../h/pmc.h"
#include "../h/pio.h"
#include "../h/usart.h"

int init_ser(void);
char putch(char);
char getch(void);

#define 	DEFAULT_BAUD 38400 // mit Einstellungen in Minicom abgleichen
#define 	CLOCK_SPEED 25000000
//US_BAUD =  (CLOCK_SPEED / (16*(DEFAULT_BAUD))	// 25MHz / ( 16 * 38400) = 40.69  -> 41 -> 0x29
#define 	US_BAUD 0x29   

// Initialisiert die serielle Schnittstelle USART0

int init_ser() 
{
	StructPIO* piobaseA = PIOA_BASE;	
	StructPMC* pmcbase  = PMC_BASE;	
	StructUSART* usartbase0 = USART0;	
	
	pmcbase->PMC_PCER = 0x4;	// Clock f�r US0 einschalten
	piobaseA->PIO_PDR = 0x18000;	// US0 TxD und RxD 
	usartbase0->US_CR = 0xa0;	// TxD und RxD disable
	usartbase0->US_BRGR = US_BAUD;	// Baud Rate Generator Register 
	usartbase0->US_MR = 0x8c0;	// Keine Parit�t, 8 Bit, MCKI -> Mode Register S.82 in Handbuch AT91 Complete
	usartbase0->US_CR = 0x50;	// TxD und RxD enable

	return 0;
}

// Gibt wenn m�glich ein Zeichen auf die serielle Schnittstelle aus
// und liefert das Zeichen wieder zur�ck
// wenn eine Ausgabe nicht m�glich war wird eine 0 zur�ck geliefert

char putch(char Zeichen) 
{
	StructUSART* usartbase0 = USART0;	

	if( usartbase0->US_CSR & US_TXRDY ) // Channel Status Register verunden mit Transmitter ready
	{
		usartbase0->US_THR = Zeichen; // Transmit Holding register bekommt char zugewiesen -> n�chstes zu sendendes Wort
	}
	else
	{
		Zeichen = 0;	// wenn keine Ausgabe
	}
	return Zeichen; 
}

// Gibt entweder ein empfangenes Zeichen oder eine 0 zur�ck
char getch(void) 
{
	StructUSART* usartbase0 = USART0;	
	char Zeichen;

	if( usartbase0->US_CSR & US_RXRDY ) // csr & Receiver Ready
	{
		Zeichen = usartbase0->US_RHR; // Receive Shift Register -> aktuell empfangenes Wort
	}
	else 
	{
		Zeichen = 0;
	}

	return Zeichen;
	
}


void convertInt(int zahl)
{
    if(zahl == 0x80000000){
        puts("-214783648");
        return;
    }
    char result[12];
    result[11] = 0;
    int i;
    int neg = 0;
    
    if(zahl<0){
        zahl *= -1;
        neg = 1;
    }
    for(i = 10; i>0; i--){
        result[i] = (zahl%10)+ '0';         //0x30 w�re auch m�glich 
    
        zahl /=10;
        if(zahl == 0){
            break;            
        }
    }
    if(neg == 1){
        i--;
        result[i] = '-';
    }
    puts(&result[i]);
}


