// Programmrahmen zur Aufgabe Termin5
// Aufgabe 1
//************************************
// 
// von: Manfred Pester
// vom: 06. August 2003
// letzte �nderung:	30. November 2004
// von: Manfred Pester

//#include "seriell.h"

int main(void)
{
//	char i;
// Serielle initialisieren  
	inits();
//	init_ser();
// CR (Carriage Return) und LF (Line Feed) auf das Terminal ausgeben
//	putc (0xd); // erwarten  \r
    puts ("Hallo\n");
	puts ("\n");    // erwarten \n in minicom
    convertInt(-2000);
//    putch (0xa);
// ein Zeichen von der seriellen abholen	
//	i=getc();  // in minicom Zeichen �ber Tastatur eingeben -> �ber Debugger in i zu sehen
//	putc(i);   // eingegebenes Zeichen ist in Minicom zu erwarten
// String ausgeben
//	puts("Hallo! \n");
	
	return 0;
}
