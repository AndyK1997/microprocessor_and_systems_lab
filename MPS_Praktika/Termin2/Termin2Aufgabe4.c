// Loesung zu Termin2
// Aufgabe 4
// Namen: __________; ___________
// Matr.: __________; ___________
// vom :  __________
// 

#include "../h/pmc.h"
#include "../h/pio.h"
#include "../h/aic.h"

void taste_irq_handler (void) __attribute__ ((interrupt));

void taste_irq_handler (void)
{
    StructPIO* piobaseB = PIOB_BASE; // Basisadresse PIO B
    StructAIC* aicbase = AIC_BASE; // Basisadresse AIC
    
    // Inhalt der Interrupt Service Routine
    if (!(piobaseB->PIO_PDSR & KEY1)) {  
            piobaseB->PIO_CODR = LED1;  // Ausgabe Register leeren (AN, weil low active)
    }
    if (!(piobaseB->PIO_PDSR & KEY2)) {
            piobaseB->PIO_SODR = LED1;  // Ausgabe Register setzen (AUS)
    }
    
    // InterruptStatusRegister lesen und in End of Interrupt Command Register (EOICR) schreiben
    aicbase->AIC_EOICR = piobaseB->PIO_ISR; // WICHTIG um Routine zu beenden!
}

int main(void)
{
    StructPMC* pmcbase = PMC_BASE;	// Basisadresse des PMC
    StructPIO* piobaseB = PIOB_BASE;	// Basisadresse PIOB
    
    // Interruptanweisung:
    StructAIC* aicbase = AIC_BASE; // Basisadresse AIC
  
    // Peripheral Clock:
    pmcbase->PMC_PCER = 0x00004000; // Bit 14 setzen
    
    // Aktivieren:
    piobaseB->PIO_PER = KEY1 | KEY2 | LED1 | LED2; // Ports aktivieren
    piobaseB->PIO_OER = LED1 | LED2;     // Ausgabe aktivieren
    
    piobaseB->PIO_SODR = LED1 | LED2; // LEDs ausschalten
    
    //Interruptanweisungen:
    aicbase->AIC_IDCR = 0x4000; // Interrupt PIOB ausschalten (1<<14)
    aicbase->AIC_ICCR = 0x4000; // Interrupt PIOB löschen
    
    aicbase->AIC_SVR[PIOB_ID] = (unsigned int)taste_irq_handler; // SourceVectorRegister angeben->interrupt Routine
    //aicbase->AIC_SMR[PIOB_ID] = 0x7; // SourceModerRegister Priorität angeben
    aicbase->AIC_IECR = 0x4000; // Interrupt PIOB einschalten
    
    // Interrupt durch KEY1 und KEY2 erlauben:
    piobaseB->PIO_IER = KEY1 | KEY2;
    
  
  while(1)
  {
    
    int i = 0;
    while(i <= 250000){
        /*
         * if (!(piobaseB->PIO_PDSR & KEY1)) {  
            piobaseB->PIO_CODR = LED1;  // Ausgabe Register leeren (AN, weil low active)
        }
        if (!(piobaseB->PIO_PDSR & KEY2)) {
            piobaseB->PIO_SODR = LED1;  // Ausgabe Register setzen (AUS)
        }*/
        i++;
    }
    
    piobaseB->PIO_CODR = LED2;
    
    i = 0;
    
    while(i <= 250000){
       /*
       if (!(piobaseB->PIO_PDSR & KEY1)) {  
            piobaseB->PIO_CODR = LED1;  // Ausgabe Register leeren (AN, weil low active)
        }
        if (!(piobaseB->PIO_PDSR & KEY2)) {
            piobaseB->PIO_SODR = LED1;  // Ausgabe Register setzen (AUS)
        }*/
        i++;
    }
    
    piobaseB->PIO_SODR = LED2;
  }
  
  aicbase->AIC_IDCR = 1 << 14; // Interrupt PIOB ausschalten
  aicbase->AIC_ICCR = 1 << 14; // Interrupt PIOB löschen
    
    
	return 0;
}
