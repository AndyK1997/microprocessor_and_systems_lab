	.file	"Termin2Aufgabe3.c"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #12
	mov	r3, #-1610612736
	mov	r3, r3, asr #15
	str	r3, [fp, #-12]
	mov	r3, #-2147483648
	mov	r3, r3, asr #15
	str	r3, [fp, #-8]
	ldr	r3, [fp, #-12]
	mov	r2, #16384
	str	r2, [r3, #16]
	ldr	r3, [fp, #-8]
	mov	r2, #792
	str	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	mov	r2, #768
	str	r2, [r3, #16]
	ldr	r3, [fp, #-8]
	mov	r2, #768
	str	r2, [r3, #48]
.L10:
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L2
.L5:
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	and	r3, r3, #8
	cmp	r3, #0
	bne	.L3
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #52]
.L3:
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	and	r3, r3, #16
	cmp	r3, #0
	bne	.L4
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #48]
.L4:
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L2:
	ldr	r2, [fp, #-4]
	mov	r3, #249856
	add	r3, r3, #144
	cmp	r2, r3
	ble	.L5
	ldr	r3, [fp, #-8]
	mov	r2, #512
	str	r2, [r3, #52]
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L6
.L9:
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	and	r3, r3, #8
	cmp	r3, #0
	bne	.L7
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #52]
.L7:
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #60]
	and	r3, r3, #16
	cmp	r3, #0
	bne	.L8
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #48]
.L8:
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L6:
	ldr	r2, [fp, #-4]
	mov	r3, #249856
	add	r3, r3, #144
	cmp	r2, r3
	ble	.L9
	ldr	r3, [fp, #-8]
	mov	r2, #512
	str	r2, [r3, #48]
	b	.L10
	.size	main, .-main
	.ident	"GCC: (crosstool-NG 1.13.3 - arm-elf) 4.4.6"
