// Loesung zu Termin2
// Aufgabe 3
// von: Andreas Kirchner
// von: Stefan Jaeger
// 

#include "../h/pmc.h"
#include "../h/pio.h"

int main(void)
{
  StructPMC* pmcbase = PMC_BASE;	// Basisadresse des PMC
  StructPIO* piobaseB = PIOB_BASE;	// Basisadresse PIOB
  
  pmcbase->PMC_PCER = 0x00004000; // Bit 14 setzen
  
  piobaseB->PIO_PER = KEY1 | KEY2 | LED1 | LED2;
  piobaseB->PIO_OER = LED1 | LED2;     // Ausgabe aktivieren
  
  piobaseB->PIO_SODR = LED1 | LED2;
  
  while(1)
  {
    int i = 0;
    while(i <= 250000){
        if (!(piobaseB->PIO_PDSR & KEY1)) {  
            piobaseB->PIO_CODR = LED1;  // Ausgabe Register leeren (AN, weil low active)
        }
        if (!(piobaseB->PIO_PDSR & KEY2)) {
            piobaseB->PIO_SODR = LED1;  // Ausgabe Register setzen (AUS)
        }
        i++;
    }
    
    piobaseB->PIO_CODR = LED2;
    i = 0;
    
    while(i <= 250000){
        if (!(piobaseB->PIO_PDSR & KEY1)) {  
            piobaseB->PIO_CODR = LED1;  // Ausgabe Register leeren (AN, weil low active)
        }
        if (!(piobaseB->PIO_PDSR & KEY2)) {
            piobaseB->PIO_SODR = LED1;  // Ausgabe Register setzen (AUS)
        }
        i++;
    }
    
    piobaseB->PIO_SODR = LED2;
  }
    
  return 0;
}
