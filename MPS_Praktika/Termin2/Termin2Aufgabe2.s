	.file	"Termin2Aufgabe2.c"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #8
	mov	r3, #-1610612736
	mov	r3, r3, asr #15
	str	r3, [fp, #-8]
	mov	r3, #-2147483648
	mov	r3, r3, asr #15
	str	r3, [fp, #-4]
	ldr	r3, [fp, #-8]
	mov	r2, #16384
	str	r2, [r3, #16]
	ldr	r3, [fp, #-4]
	mov	r2, #312
	str	r2, [r3, #0]
	ldr	r3, [fp, #-4]
	mov	r2, #256
	str	r2, [r3, #16]
	ldr	r3, [fp, #-4]
	mov	r2, #256
	str	r2, [r3, #48]
	b	.L4
.L6:
	mov	r0, r0	@ nop
.L4:
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #60]
	and	r3, r3, #8
	cmp	r3, #0
	bne	.L2
	ldr	r3, [fp, #-4]
	mov	r2, #256
	str	r2, [r3, #52]
.L2:
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #60]
	and	r3, r3, #16
	cmp	r3, #0
	bne	.L6
	ldr	r3, [fp, #-4]
	mov	r2, #256
	str	r2, [r3, #48]
	b	.L4
	.size	main, .-main
	.ident	"GCC: (crosstool-NG 1.13.3 - arm-elf) 4.4.6"
