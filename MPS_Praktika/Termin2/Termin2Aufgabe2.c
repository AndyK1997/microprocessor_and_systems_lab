// Loesung zu Termin2
// Aufgabe 2
// von: Andreas Kirchner
// von: Stefan Jaeger
// 

#include "../h/pmc.h"
#include "../h/pio.h"

int main(void)
{
  StructPMC* pmcbase = PMC_BASE;	// Basisadresse des PMC
  StructPIO* piobaseB = PIOB_BASE;	// Basisadresse PIOB
  
  pmcbase->PMC_PCER = 0x00004000; // Bit 14 setzen
  
  piobaseB->PIO_PER = ALL_KEYS | LED1;
  piobaseB->PIO_OER = LED1;     // Ausgabe aktivieren
  
  piobaseB->PIO_SODR = LED1;
  
  while(1)
  {
    if (!(piobaseB->PIO_PDSR & KEY1)) {  
        piobaseB->PIO_CODR = LED1;  // Ausgabe Register leeren (AN, weil low active)
    }
    if (!(piobaseB->PIO_PDSR & KEY2)) {
        piobaseB->PIO_SODR = LED1;  // Ausgabe Register setzen (AUS)
    }
  }
    
  return 0;
}
