	.file	"Termin1Aufgabe6l.c"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #16
	mov	r3, #-2147483648
	mov	r3, r3, asr #15
	str	r3, [fp, #-16]
	mvn	r3, #65280
	sub	r3, r3, #239
	str	r3, [fp, #-12]
	mvn	r3, #65280
	sub	r3, r3, #207
	str	r3, [fp, #-8]
	mvn	r3, #65280
	sub	r3, r3, #203
	str	r3, [fp, #-4]
	ldr	r3, [fp, #-16]
	mov	r2, #256
	str	r2, [r3, #0]
	ldr	r3, [fp, #-12]
	mov	r2, #256
	str	r2, [r3, #0]
.L2:
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #0]
	ldr	r3, [fp, #-4]
	mov	r2, #256
	str	r2, [r3, #0]
	b	.L2
	.size	main, .-main
	.ident	"GCC: (crosstool-NG 1.13.3 - arm-elf) 4.4.6"
