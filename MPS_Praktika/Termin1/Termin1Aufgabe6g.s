	.file	"Termin1Aufgabe6g.c"
	.global	adr_PIOB_PER
	.data
	.align	2
	.type	adr_PIOB_PER, %object
	.size	adr_PIOB_PER, 4
adr_PIOB_PER:
	.word	-65536
	.global	adr_PIOB_OER
	.align	2
	.type	adr_PIOB_OER, %object
	.size	adr_PIOB_OER, 4
adr_PIOB_OER:
	.word	-65520
	.global	adr_PIOB_SODR
	.align	2
	.type	adr_PIOB_SODR, %object
	.size	adr_PIOB_SODR, 4
adr_PIOB_SODR:
	.word	-65488
	.global	adr_PIOB_CODR
	.align	2
	.type	adr_PIOB_CODR, %object
	.size	adr_PIOB_CODR, 4
adr_PIOB_CODR:
	.word	-65484
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	ldr	r3, .L4
	ldr	r3, [r3, #0]
	mov	r2, #256
	str	r2, [r3, #0]
	ldr	r3, .L4+4
	ldr	r3, [r3, #0]
	mov	r2, #256
	str	r2, [r3, #0]
.L2:
	ldr	r3, .L4+8
	ldr	r3, [r3, #0]
	mov	r2, #256
	str	r2, [r3, #0]
	ldr	r3, .L4+12
	ldr	r3, [r3, #0]
	mov	r2, #256
	str	r2, [r3, #0]
	b	.L2
.L5:
	.align	2
.L4:
	.word	adr_PIOB_PER
	.word	adr_PIOB_OER
	.word	adr_PIOB_SODR
	.word	adr_PIOB_CODR
	.size	main, .-main
	.ident	"GCC: (crosstool-NG 1.13.3 - arm-elf) 4.4.6"
