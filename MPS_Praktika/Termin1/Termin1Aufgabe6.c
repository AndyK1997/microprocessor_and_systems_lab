// Loesung zu Termin1
// Aufgabe 6
// Namen: Andreas Kirchner; Stefan Jäger
// Matr.: 759142; 747090
// vom:   06.11.19

// Beispiel des Anlegens und der Nutzung einer Zeigervariablen
#define PIOB_PER ((unsigned int *) 0xFFFF0000)
#define PIOB_OER ((unsigned int *) 0xFFFF0010)
#define PIOB_SODR ((unsigned int *) 0xFFFF0030)
#define PIOB_CODR ((unsigned int *) 0xFFFF0034)

int main (void)
{
    *PIOB_PER = 0x100; // Port aktivieren
    *PIOB_OER = 0x100; // Ausgabe aktivieren

    while (1) {
        *PIOB_SODR = 0x100; // Ausgabe Register setzen
        *PIOB_CODR = 0x100; // Ausgabe Register leeren
    }

    return (0);
}
