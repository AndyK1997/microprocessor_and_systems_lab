	.file	"Termin1Aufgabe3.c"
	.data
	.align	2
	.type	GLOBALVAR1, %object
	.size	GLOBALVAR1, 4
GLOBALVAR1:
	.word	1
	.align	2
	.type	GLOBALVAR2, %object
	.size	GLOBALVAR2, 4
GLOBALVAR2:
	.word	2
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #8
	mov	r3, #3
	str	r3, [fp, #-8]
	mov	r3, #4
	str	r3, [fp, #-4]
	ldr	r3, .L3
	ldr	r2, [fp, #-8]
	str	r2, [r3, #0]
	ldr	r3, .L3+4
	ldr	r3, [r3, #0]
	str	r3, [fp, #-4]
	mov	r3, #0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L4:
	.align	2
.L3:
	.word	GLOBALVAR1
	.word	GLOBALVAR2
	.size	main, .-main
	.ident	"GCC: (crosstool-NG 1.13.3 - arm-elf) 4.4.6"
