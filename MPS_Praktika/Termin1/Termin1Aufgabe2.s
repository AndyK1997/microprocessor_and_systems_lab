	.file	"Termin1Aufgabe2.c"
	.bss
	.align	2
GLOBALVAR1:
	.space	4
	.align	2
GLOBALVAR2:
	.space	4
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	ldr	r3, .L3
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L3+4
	mov	r2, #2
	str	r2, [r3, #0]
	mov	r3, #0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L4:
	.align	2
.L3:
	.word	GLOBALVAR1
	.word	GLOBALVAR2
	.size	main, .-main
	.ident	"GCC: (crosstool-NG 1.13.3 - arm-elf) 4.4.6"
