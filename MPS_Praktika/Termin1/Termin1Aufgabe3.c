#include <stdio.h>

static int GLOBALVAR1=0x1;
static int GLOBALVAR2=0x2;

int main() {

    int LOCALVAR1=0x3;
    int LOCALVAR2=0x4;

    GLOBALVAR1=LOCALVAR1;
    LOCALVAR2=GLOBALVAR2;
    return 0;
}