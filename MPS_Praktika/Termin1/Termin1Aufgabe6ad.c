// Loesung zu Termin1
// Aufgabe 6
// Namen: Andreas Kirchner; Stefan Jäger
// Matr.: 759142; 747090
// vom:   06.11.19

//unsigned int adr_PIOB_CODR = 0xFFFF0034;
//unsigned int adr_PIOB_SODR = 0xFFFF0030;
//unsigned int adr_PIOB_OER = 0xFFFF0010;
//unsigned int adr_PIOB_PER = 0xFFFF0000;

int main (void)
{
    *((unsigned int *) 0xffff0000) = 0x100; // Port aktivieren
    *((unsigned int *) 0xffff0010) = 0x100; // Ausgabe aktivieren

    while (1) {
        *((unsigned int *) 0xffff0030) = 0x100; // Ausgabe Register setzen
        *((unsigned int *) 0xffff0034) = 0x100; // Ausgabe Register leeren
    }

    return (0);
}
