	.file	"Termin1Aufgabe6.c"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	mov	r3, #-2147483648
	mov	r3, r3, asr #15
	mov	r2, #256
	str	r2, [r3, #0]
	mvn	r3, #65280
	sub	r3, r3, #239
	mov	r2, #256
	str	r2, [r3, #0]
.L2:
	mvn	r3, #65280
	sub	r3, r3, #207
	mov	r2, #256
	str	r2, [r3, #0]
	mvn	r3, #65280
	sub	r3, r3, #203
	mov	r2, #256
	str	r2, [r3, #0]
	b	.L2
	.size	main, .-main
	.ident	"GCC: (crosstool-NG 1.13.3 - arm-elf) 4.4.6"
