// Loesung zu Termin1
// Aufgabe 6
// Namen: Andreas Kirchner; Stefan Jäger
// Matr.: 759142; 747090
// vom:   06.11.19

// Variable mit der Adresse fuer PIOB_PER    
  unsigned int adr_PIOB_PER = 0xFFFF0000;  
// Variable mit der Adresse fuer PIOB_OER
  unsigned int adr_PIOB_OER = 0xFFFF0010;
// Variable mit der Adresse fuer PIOB_SODR;
  unsigned int adr_PIOB_SODR = 0xFFFF0030;
//
  unsigned int adr_PIOB_CODR = 0xFFFF0034;

int main (void)
{
    *((unsigned int *) adr_PIOB_PER) = 0x100; // Port aktivieren
    *((unsigned int *) adr_PIOB_OER) = 0x100; // Ausgabe aktivieren

    while (1) {
        *((unsigned int *) adr_PIOB_SODR) = 0x100; // Ausgabe Register setzen
        *((unsigned int *) adr_PIOB_CODR) = 0x100; // Ausgabe Register leeren
    }

    return (0);
}
