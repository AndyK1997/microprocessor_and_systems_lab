/*----------------------------------------------------------------------------
@ File Name     : swi.c
@ Object        : SoftwareInterruptHandler
@
@ Autor 	: Horsch/Pester/Steiger
@ Datum		: 3.12.2007/Januar2011/April2011
@----------------------------------------------------------------------------*/
void SWIHandler () __attribute__ ((interrupt ("SWI")));

void SWIHandler() 
{
    // Erklärung in Mitschrift termin5
	register char *reg_r0 asm ("r0"); // asm -> Assembler
	register unsigned int *reg_14 asm ("r14");

	switch( *(reg_14 - 1) & 0x00FFFFFF) // Ausblenden der oberen 8 Bits 
					    // und Verzweigen in Abh. der SWI-Nummer
	{ 
		case 0x100:   // serielle Schittstelle initialisieren
			init_ser();
			break;
		case 0x200:   // zeichen in Transmitter Holding Register ausgeben
			*reg_r0 = putch(*reg_r0);
			break;
		case 0x300:   // zeichen von Receiver Register empfangen
			*reg_r0 = getch();  
			break;
	}
}

