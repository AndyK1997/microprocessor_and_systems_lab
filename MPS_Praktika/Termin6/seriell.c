#include "../h/pmc.h"
#include "../h/pio.h"
#include "../h/usart.h"

#include "../h/tc.h"
#include "math.h"

int init_ser(void);
char putch(char);
char getch(void);

double masseBecher = 0.0;
double MasseOhneBecher =0.0;

#define 	DEFAULT_BAUD 38400 // mit Einstellungen in Minicom abgleichen 57600
#define 	CLOCK_SPEED 25000000
                        //US_BAUD =  (CLOCK_SPEED / (16*(DEFAULT_BAUD))	// 25MHz / ( 16 * 38400) = 40.69  -> 41 -> 0x29  1b
#define 	US_BAUD 0x29   

#define     C1 2000//18030
#define     C2 0//40

// f�r die Initialisierung des Z�hler TC4
#define TC4_INIT  TC_CLKS_MCK2 | TC_LDBSTOP | TC_CAPT | TC_LDRA_RISING_EDGE | TC_LDRB_RISING_EDGE
//                  0x0             0x40        0x0         0x10000               0x40000
#define TC5_INIT  TC_CLKS_MCK2 | TC_LDBSTOP | TC_CAPT | TC_LDRA_RISING_EDGE | TC_LDRB_RISING_EDGE


    StructTC* tcbase4 = TCB4_BASE;
	StructTC* tcbase5 = TCB5_BASE;
    
    volatile double	captureRA1;
	volatile double	captureRB1;
	volatile double	capturediff1;
    volatile double	captureRA2;
	volatile double	captureRB2;
	volatile double	capturediff2;
    
    volatile double Periodendauer1;
    volatile double Periodendauer2;
    
    
void PIO_Init(void)
{
    StructPIO* piobaseA = PIOA_BASE;
    StructPIO* piobaseB = PIOB_BASE;
    piobaseA->PIO_PDR	=	0x090;		//write only, Timer4 und Timer5 aktivieren
    piobaseB->PIO_PER = ALL_LEDS;
    piobaseB->PIO_OER = ALL_LEDS;     // Ausgabe aktivieren
  
    piobaseB->PIO_SODR = ALL_LEDS;
}

void Timer_Init(void)
{
    tcbase4->TC_CCR		=	TC_CLKDIS;  //0x2 -> CounterClockDisableCommand = 1 -> disables the clock
	tcbase4->TC_CMR		=	TC4_INIT;   //ChannelModeRegister S.116; TC4_INIT oben als define
	tcbase4->TC_CCR		=	TC_CLKEN;   //0x1 -> CounterClockEnableCommand = 1 -> enables the clock, if TC_CLKDIS !=1
	tcbase4->TC_CCR		=	TC_SWTRG;   //0x4 -> Softw.TriggerCmd=1 -> softw.trigger is performed (counter reset & clock started)
	
	tcbase5->TC_CCR = TC_CLKDIS;
    tcbase5->TC_CMR = TC5_INIT;
    tcbase5->TC_CCR = TC_CLKEN;
    tcbase5->TC_CCR = TC_SWTRG;
}

// Initialisiert die serielle Schnittstelle USART0

int init_ser() 
{
	StructPIO* piobaseA = PIOA_BASE;	
	StructPMC* pmcbase  = PMC_BASE;	
	StructUSART* usartbase0 = USART0;	
	
    pmcbase->PMC_PCER = 0x6C04;	// Clock fuer US0, PIOA, PIOB, Timer4, Timer5 einschalten
	piobaseA->PIO_PDR = 0x18000;	// US0 TxD und RxD 
	usartbase0->US_CR = 0xa0;	// TxD und RxD disable
	usartbase0->US_BRGR = US_BAUD;	// Baud Rate Generator Register 
	usartbase0->US_MR = 0x8c0;	// Keine Parit�t, 8 Bit, MCKI -> Mode Register S.82 in Handbuch AT91 Complete 8c0 -> waage 2c0
	usartbase0->US_CR = 0x50;	// TxD und RxD enable

	return 0;
}

// Gibt wenn m�glich ein Zeichen auf die serielle Schnittstelle aus
// und liefert das Zeichen wieder zur�ck
// wenn eine Ausgabe nicht m�glich war wird eine 0 zur�ck geliefert

char putch(char Zeichen) 
{
	StructUSART* usartbase0 = USART0;	

	if( usartbase0->US_CSR & US_TXRDY ) // Channel Status Register verunden mit Transmitter ready
	{
		usartbase0->US_THR = Zeichen; // Transmit Holding register bekommt char zugewiesen -> n�chstes zu sendendes Wort
	}
	else
	{
		Zeichen = 0;	// wenn keine Ausgabe
	}
	return Zeichen; 
}

// Gibt entweder ein empfangenes Zeichen oder eine 0 zur�ck
char getch(void) 
{
	StructUSART* usartbase0 = USART0;	
	char Zeichen;

	if( usartbase0->US_CSR & US_RXRDY ) // csr & Receiver Ready
	{
		Zeichen = usartbase0->US_RHR; // Receive Shift Register -> aktuell empfangenes Wort
	}
	else 
	{
		Zeichen = 0;
	}

	return Zeichen;
	
}


void convertInt(int zahl)
{
    if(zahl == 0x80000000){
        puts("-214783648");
        return;
    }
    char result[12];
    result[11] = 0;
    int i;
    int neg = 0;
    
    if(zahl<0){
        zahl *= -1;
        neg = 1;
    }
    for(i = 10; i>0; i--){
        result[i] = (zahl%10)+ '0';         //0x30 w�re auch m�glich 
    
        zahl /=10;
        if(zahl == 0){
            break;            
        }
    }
    if(neg == 1){
        i--;
        result[i] = '-';
    }
    puts(&result[i]);
}

double MessungDerMasse(void)
{
    tcbase4->TC_CCR	=	TC_SWTRG; // ControlRegister bekommt SoftwareTrigger (f�r Clock4)
    tcbase5->TC_CCR =   TC_SWTRG;
        
    while (!(tcbase4->TC_SR & 0x40));		// Status Register mit 0x40 f�r LDRBS abgleichen 
                                            // Capture Register B wurde geladen Messung abgeschlossen
    captureRA1	= tcbase4->TC_RA;	//Timer Register A speichern
    captureRB1	= tcbase4->TC_RB;     // Timer Register B speichern
    capturediff1	= fabs(captureRB1) - fabs(captureRA1);    // differenz = Absolute,positive WertB - WertA
    //Periodendauer1 = fabs(capturediff1) / 12.5;	// Periodendauer = differenz/12,5us
    while (!(tcbase5->TC_SR & 0x40));
    captureRA2 = tcbase5->TC_RA;
    captureRB2 = tcbase5->TC_RB;
    capturediff2 = fabs(captureRB2)-fabs(captureRA2);
    //Periodendauer2 = fabs(capturediff2) / 12.5;
/*wir teilen die Differenz durch 12,5us weil: prozessortakt: 25mhz -> 25mhz/2 = 12,5*10^6 hz*/
    double Ergebnis = (C1*((capturediff1/capturediff2)-1)-C2);
    //float Ergebnis = (2000*((Periodendauer1/Periodendauer2)-1)-0);
    return fabs(Ergebnis);
}

//
//Funktion zum wiegen des Tara
//
int wiegeBecher(void)
{
    volatile double masse = 0;
    StructPIO* piobaseA = PIOA_BASE;
    
    MessungDerMasse();
    int cnt = 0;
    while(cnt <= 49)
	{
		masse += MessungDerMasse();
        cnt++;
	}
	masse = masse / cnt;
    masseBecher = masse;
    
    /*int rueckgabe = masse;
    float komstell = fabs(masse - rueckgabe);
    
    if (komstell < 0.5)
    {
        return rueckgabe;
    }else if (komstell >= 0.5)
    {
        float rund=1.0-komstell;
        masse += rund;
        return ((int)masse);
    }*/
    return Rundung(masse);
}

//
//Funktion zum wiegen des M�nzen gewichts
//
int wiegeMuenzen(void)
{
    volatile double masse = 0;
    
    MessungDerMasse();
    int cnt = 0;
    while(cnt <= 129)
	{
		masse += (MessungDerMasse()-masseBecher);
        cnt++;
	}
	masse = masse / (double)cnt;
    MasseOhneBecher=masse;
    
    return Rundung(masse);
}


//
//Funktion zum berechnen der Anzahl der Muenzen 
//
int berechnungAnzahlMuenzen(double muenzGewicht){
        volatile double masse = 0;
    
    MessungDerMasse();
    int cnt = 0;
    while(cnt <= 129)
	{
		masse += (MessungDerMasse()-masseBecher);
        cnt++;
	}
	masse = masse / (double)cnt;
    
    double Anzahl = masse/muenzGewicht;
    
   return Rundung(Anzahl);
}


void binAusgabeMuenzen(int Anzahl)
{
    StructPIO* piobaseB = PIOB_BASE;
    
    int a[8]= {0};
        
    int i = 0;
    while(Anzahl>0)    
    {    
        a[i]=Anzahl%2;    
        Anzahl=Anzahl/2;
        i++;
    }    


    if(a[0]==1){
        piobaseB->PIO_CODR = LED8;  // Ausgabe Register leeren (AN, weil low active)
    } else {
        piobaseB->PIO_SODR = LED8;  // Ausgabe Register setzen (AUS)
    }
    
        if(a[1]==1){
        piobaseB->PIO_CODR = LED7;  // Ausgabe Register leeren (AN, weil low active)
    } else {
        piobaseB->PIO_SODR = LED7;  // Ausgabe Register setzen (AUS)
    }
    
        if(a[2]==1){
        piobaseB->PIO_CODR = LED6;  // Ausgabe Register leeren (AN, weil low active)
    } else {
        piobaseB->PIO_SODR = LED6;  // Ausgabe Register setzen (AUS)
    }
    
        if(a[3]==1){
        piobaseB->PIO_CODR = LED5;  // Ausgabe Register leeren (AN, weil low active)
    } else {
        piobaseB->PIO_SODR = LED5;  // Ausgabe Register setzen (AUS)
    }
    
        if(a[4]==1){
        piobaseB->PIO_CODR = LED4;  // Ausgabe Register leeren (AN, weil low active)
    } else {
        piobaseB->PIO_SODR = LED4;  // Ausgabe Register setzen (AUS)
    }
    
        if(a[5]==1){
        piobaseB->PIO_CODR = LED3;  // Ausgabe Register leeren (AN, weil low active)
    } else {
        piobaseB->PIO_SODR = LED3;  // Ausgabe Register setzen (AUS)
    }
    
        if(a[6]==1){
        piobaseB->PIO_CODR = LED2;  // Ausgabe Register leeren (AN, weil low active)
    } else {
        piobaseB->PIO_SODR = LED2;  // Ausgabe Register setzen (AUS)
    }
    
        if(a[7]==1){
        piobaseB->PIO_CODR = LED1;  // Ausgabe Register leeren (AN, weil low active)
    } else {
        piobaseB->PIO_SODR = LED1;  // Ausgabe Register setzen (AUS)
    }
    
}


int getMasseBecher(void){
    
   return Rundung(masseBecher);
}


int getBrutto(void){
    
    double Brutto= masseBecher+MasseOhneBecher;
    
    return Rundung(Brutto);
}

int Rundung(double Wert){
    int gerundeterWert=0; 
    //gerundeterWert = (int)(Wert / 1000);
    gerundeterWert=(int)(Wert+0.5);
    return gerundeterWert;
}
