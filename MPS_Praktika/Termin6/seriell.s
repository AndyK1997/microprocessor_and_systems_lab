	.file	"seriell.c"
	.global	masseBecher
	.bss
	.align	2
	.type	masseBecher, %object
	.size	masseBecher, 8
masseBecher:
	.space	8
	.global	MasseOhneBecher
	.align	2
	.type	MasseOhneBecher, %object
	.size	MasseOhneBecher, 8
MasseOhneBecher:
	.space	8
	.global	tcbase4
	.data
	.align	2
	.type	tcbase4, %object
	.size	tcbase4, 4
tcbase4:
	.word	-180160
	.global	tcbase5
	.align	2
	.type	tcbase5, %object
	.size	tcbase5, 4
tcbase5:
	.word	-180096
	.comm	captureRA1,8,4
	.comm	captureRB1,8,4
	.comm	capturediff1,8,4
	.comm	captureRA2,8,4
	.comm	captureRB2,8,4
	.comm	capturediff2,8,4
	.comm	Periodendauer1,8,4
	.comm	Periodendauer2,8,4
	.text
	.align	2
	.global	PIO_Init
	.type	PIO_Init, %function
PIO_Init:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #8
	mov	r3, #-1342177280
	mov	r3, r3, asr #14
	str	r3, [fp, #-8]
	mov	r3, #-2147483648
	mov	r3, r3, asr #15
	str	r3, [fp, #-4]
	ldr	r3, [fp, #-8]
	mov	r2, #144
	str	r2, [r3, #4]
	ldr	r3, [fp, #-4]
	mov	r2, #65280
	str	r2, [r3, #0]
	ldr	r3, [fp, #-4]
	mov	r2, #65280
	str	r2, [r3, #16]
	ldr	r3, [fp, #-4]
	mov	r2, #65280
	str	r2, [r3, #48]
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
	.size	PIO_Init, .-PIO_Init
	.align	2
	.global	Timer_Init
	.type	Timer_Init, %function
Timer_Init:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	mov	r2, #2
	str	r2, [r3, #0]
	ldr	r3, .L5
	ldr	r2, [r3, #0]
	mov	r3, #327680
	add	r3, r3, #64
	str	r3, [r2, #4]
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	mov	r2, #4
	str	r2, [r3, #0]
	ldr	r3, .L5+4
	ldr	r3, [r3, #0]
	mov	r2, #2
	str	r2, [r3, #0]
	ldr	r3, .L5+4
	ldr	r2, [r3, #0]
	mov	r3, #327680
	add	r3, r3, #64
	str	r3, [r2, #4]
	ldr	r3, .L5+4
	ldr	r3, [r3, #0]
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L5+4
	ldr	r3, [r3, #0]
	mov	r2, #4
	str	r2, [r3, #0]
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L6:
	.align	2
.L5:
	.word	tcbase4
	.word	tcbase5
	.size	Timer_Init, .-Timer_Init
	.align	2
	.global	init_ser
	.type	init_ser, %function
init_ser:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #12
	mov	r3, #-1342177280
	mov	r3, r3, asr #14
	str	r3, [fp, #-12]
	mov	r3, #-1610612736
	mov	r3, r3, asr #15
	str	r3, [fp, #-8]
	mov	r3, #-2147483648
	mov	r3, r3, asr #13
	str	r3, [fp, #-4]
	ldr	r2, [fp, #-8]
	mov	r3, #27648
	add	r3, r3, #4
	str	r3, [r2, #16]
	ldr	r3, [fp, #-12]
	mov	r2, #98304
	str	r2, [r3, #4]
	ldr	r3, [fp, #-4]
	mov	r2, #160
	str	r2, [r3, #0]
	ldr	r3, [fp, #-4]
	mov	r2, #41
	str	r2, [r3, #32]
	ldr	r3, [fp, #-4]
	mov	r2, #2240
	str	r2, [r3, #4]
	ldr	r3, [fp, #-4]
	mov	r2, #80
	str	r2, [r3, #0]
	mov	r3, #0
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
	.size	init_ser, .-init_ser
	.align	2
	.global	putch
	.type	putch, %function
putch:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #8
	mov	r3, r0
	strb	r3, [fp, #-8]
	mov	r3, #-2147483648
	mov	r3, r3, asr #13
	str	r3, [fp, #-4]
	ldr	r3, [fp, #-4]
	ldr	r3, [r3, #20]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L10
	ldrb	r2, [fp, #-8]	@ zero_extendqisi2
	ldr	r3, [fp, #-4]
	str	r2, [r3, #28]
	b	.L11
.L10:
	mov	r3, #0
	strb	r3, [fp, #-8]
.L11:
	ldrb	r3, [fp, #-8]	@ zero_extendqisi2
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
	.size	putch, .-putch
	.align	2
	.global	getch
	.type	getch, %function
getch:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #8
	mov	r3, #-2147483648
	mov	r3, r3, asr #13
	str	r3, [fp, #-8]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #20]
	and	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L14
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #24]
	strb	r3, [fp, #-1]
	b	.L15
.L14:
	mov	r3, #0
	strb	r3, [fp, #-1]
.L15:
	ldrb	r3, [fp, #-1]	@ zero_extendqisi2
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
	.size	getch, .-getch
	.section	.rodata
	.align	2
.LC0:
	.ascii	"-214783648\000"
	.text
	.align	2
	.global	convertInt
	.type	convertInt, %function
convertInt:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #24
	str	r0, [fp, #-28]
	ldr	r3, [fp, #-28]
	cmp	r3, #-2147483648
	bne	.L18
	ldr	r0, .L28
	bl	puts
	b	.L26
.L18:
	mov	r3, #0
	strb	r3, [fp, #-13]
	mov	r3, #0
	str	r3, [fp, #-8]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bge	.L20
	ldr	r3, [fp, #-28]
	rsb	r3, r3, #0
	str	r3, [fp, #-28]
	mov	r3, #1
	str	r3, [fp, #-8]
.L20:
	mov	r3, #10
	str	r3, [fp, #-12]
	b	.L21
.L24:
	ldr	r0, [fp, #-12]
	ldr	r1, [fp, #-28]
	ldr	r3, .L28+4
	smull	r2, r3, r1, r3
	mov	r2, r3, asr #2
	mov	r3, r1, asr #31
	rsb	r2, r3, r2
	mov	r3, r2
	mov	r3, r3, asl #2
	add	r3, r3, r2
	mov	r3, r3, asl #1
	rsb	r2, r3, r1
	and	r3, r2, #255
	add	r3, r3, #48
	and	r2, r3, #255
	mvn	r3, #19
	sub	ip, fp, #4
	add	r1, ip, r0
	add	r3, r1, r3
	strb	r2, [r3, #0]
	ldr	r3, [fp, #-28]
	ldr	r2, .L28+4
	smull	r1, r2, r3, r2
	mov	r2, r2, asr #2
	mov	r3, r3, asr #31
	rsb	r3, r3, r2
	str	r3, [fp, #-28]
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	beq	.L27
.L22:
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
.L21:
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bgt	.L24
	b	.L23
.L27:
	mov	r0, r0	@ nop
.L23:
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L25
	ldr	r3, [fp, #-12]
	sub	r3, r3, #1
	str	r3, [fp, #-12]
	ldr	r2, [fp, #-12]
	mvn	r3, #19
	sub	ip, fp, #4
	add	r2, ip, r2
	add	r3, r2, r3
	mov	r2, #45
	strb	r2, [r3, #0]
.L25:
	ldr	r2, [fp, #-12]
	sub	r3, fp, #24
	add	r3, r3, r2
	mov	r0, r3
	bl	puts
.L26:
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L29:
	.align	2
.L28:
	.word	.LC0
	.word	1717986919
	.size	convertInt, .-convertInt
	.global	__floatunsidf
	.global	__subdf3
	.global	__divdf3
	.global	__muldf3
	.align	2
	.global	MessungDerMasse
	.type	MessungDerMasse, %function
MessungDerMasse:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, fp, lr}
	add	fp, sp, #28
	sub	sp, sp, #24
	ldr	r3, .L34
	ldr	r3, [r3, #0]
	mov	r2, #4
	str	r2, [r3, #0]
	ldr	r3, .L34+4
	ldr	r3, [r3, #0]
	mov	r2, #4
	str	r2, [r3, #0]
.L31:
	ldr	r3, .L34
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #32]
	and	r3, r3, #64
	cmp	r3, #0
	beq	.L31
	ldr	r3, .L34
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #20]
	mov	r0, r3
	bl	__floatunsidf
	mov	r2, r0
	mov	r3, r1
	ldr	r1, .L34+8
	stmia	r1, {r2-r3}
	ldr	r3, .L34
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #24]
	mov	r0, r3
	bl	__floatunsidf
	mov	r2, r0
	mov	r3, r1
	ldr	r1, .L34+12
	stmia	r1, {r2-r3}
	ldr	r3, .L34+12
	ldmia	r3, {r2-r3}
	bic	r1, r2, #-2147483648
	str	r1, [fp, #-52]
	mov	r1, r3
	str	r1, [fp, #-48]
	ldr	r3, .L34+8
	ldmia	r3, {r2-r3}
	bic	r1, r2, #-2147483648
	str	r1, [fp, #-44]
	mov	r1, r3
	str	r1, [fp, #-40]
	sub	r1, fp, #52
	ldmia	r1, {r0-r1}
	sub	r3, fp, #44
	ldmia	r3, {r2-r3}
	bl	__subdf3
	mov	r2, r0
	mov	r3, r1
	ldr	r1, .L34+16
	stmia	r1, {r2-r3}
.L32:
	ldr	r3, .L34+4
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #32]
	and	r3, r3, #64
	cmp	r3, #0
	beq	.L32
	ldr	r3, .L34+4
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #20]
	mov	r0, r3
	bl	__floatunsidf
	mov	r2, r0
	mov	r3, r1
	ldr	r1, .L34+20
	stmia	r1, {r2-r3}
	ldr	r3, .L34+4
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #24]
	mov	r0, r3
	bl	__floatunsidf
	mov	r2, r0
	mov	r3, r1
	ldr	r1, .L34+24
	stmia	r1, {r2-r3}
	ldr	r3, .L34+24
	ldmia	r3, {r2-r3}
	bic	r8, r2, #-2147483648
	mov	r9, r3
	ldr	r3, .L34+20
	ldmia	r3, {r2-r3}
	bic	r6, r2, #-2147483648
	mov	r7, r3
	mov	r0, r8
	mov	r1, r9
	mov	r2, r6
	mov	r3, r7
	bl	__subdf3
	mov	r2, r0
	mov	r3, r1
	ldr	r1, .L34+28
	stmia	r1, {r2-r3}
	ldr	r3, .L34+16
	ldmia	r3, {r0-r1}
	ldr	r3, .L34+28
	ldmia	r3, {r2-r3}
	bl	__divdf3
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	mov	r2, #1069547520
	add	r2, r2, #3145728
	mov	r3, #0
	bl	__subdf3
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	mov	r2, #1073741824
	add	r2, r2, #10420224
	add	r2, r2, #16384
	mov	r3, #0
	bl	__muldf3
	mov	r2, r0
	mov	r3, r1
	str	r2, [fp, #-36]
	str	r3, [fp, #-32]
	ldr	r3, [fp, #-36]
	bic	r4, r3, #-2147483648
	ldr	r5, [fp, #-32]
	mov	r3, r4
	mov	r4, r5
	mov	r0, r3
	mov	r1, r4
	sub	sp, fp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, fp, pc}
.L35:
	.align	2
.L34:
	.word	tcbase4
	.word	tcbase5
	.word	captureRA1
	.word	captureRB1
	.word	capturediff1
	.word	captureRA2
	.word	captureRB2
	.word	capturediff2
	.size	MessungDerMasse, .-MessungDerMasse
	.global	__adddf3
	.global	__floatsidf
	.align	2
	.global	wiegeBecher
	.type	wiegeBecher, %function
wiegeBecher:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, fp, lr}
	add	fp, sp, #16
	sub	sp, sp, #16
	mov	r3, #0
	mov	r4, #0
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
	mov	r3, #-1342177280
	mov	r3, r3, asr #14
	str	r3, [fp, #-24]
	bl	MessungDerMasse
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L37
.L38:
	bl	MessungDerMasse
	mov	r2, r1
	mov	r1, r0
	sub	r4, fp, #32
	ldmia	r4, {r3-r4}
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4
	bl	__adddf3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L37:
	ldr	r3, [fp, #-20]
	cmp	r3, #49
	ble	.L38
	sub	r6, fp, #32
	ldmia	r6, {r5-r6}
	ldr	r0, [fp, #-20]
	bl	__floatsidf
	mov	r3, r0
	mov	r4, r1
	mov	r0, r5
	mov	r1, r6
	mov	r2, r3
	mov	r3, r4
	bl	__divdf3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-32]
	str	r4, [fp, #-28]
	sub	r4, fp, #32
	ldmia	r4, {r3-r4}
	ldr	r2, .L40
	stmia	r2, {r3-r4}
	sub	r4, fp, #32
	ldmia	r4, {r3-r4}
	mov	r0, r3
	mov	r1, r4
	bl	Rundung
	mov	r3, r0
	mov	r0, r3
	sub	sp, fp, #16
	ldmfd	sp!, {r4, r5, r6, fp, pc}
.L41:
	.align	2
.L40:
	.word	masseBecher
	.size	wiegeBecher, .-wiegeBecher
	.align	2
	.global	wiegeMuenzen
	.type	wiegeMuenzen, %function
wiegeMuenzen:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, fp, lr}
	add	fp, sp, #16
	sub	sp, sp, #12
	mov	r3, #0
	mov	r4, #0
	str	r3, [fp, #-28]
	str	r4, [fp, #-24]
	bl	MessungDerMasse
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L43
.L44:
	bl	MessungDerMasse
	mov	r2, r1
	mov	r1, r0
	ldr	r3, .L46
	ldmia	r3, {r3-r4}
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4
	bl	__subdf3
	mov	r3, r0
	mov	r4, r1
	mov	r1, r3
	mov	r2, r4
	sub	r4, fp, #28
	ldmia	r4, {r3-r4}
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4
	bl	__adddf3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-28]
	str	r4, [fp, #-24]
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L43:
	ldr	r3, [fp, #-20]
	cmp	r3, #129
	ble	.L44
	sub	r6, fp, #28
	ldmia	r6, {r5-r6}
	ldr	r0, [fp, #-20]
	bl	__floatsidf
	mov	r3, r0
	mov	r4, r1
	mov	r0, r5
	mov	r1, r6
	mov	r2, r3
	mov	r3, r4
	bl	__divdf3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-28]
	str	r4, [fp, #-24]
	sub	r4, fp, #28
	ldmia	r4, {r3-r4}
	ldr	r2, .L46+4
	stmia	r2, {r3-r4}
	sub	r4, fp, #28
	ldmia	r4, {r3-r4}
	mov	r0, r3
	mov	r1, r4
	bl	Rundung
	mov	r3, r0
	mov	r0, r3
	sub	sp, fp, #16
	ldmfd	sp!, {r4, r5, r6, fp, pc}
.L47:
	.align	2
.L46:
	.word	masseBecher
	.word	MasseOhneBecher
	.size	wiegeMuenzen, .-wiegeMuenzen
	.align	2
	.global	berechnungAnzahlMuenzen
	.type	berechnungAnzahlMuenzen, %function
berechnungAnzahlMuenzen:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, fp, lr}
	add	fp, sp, #16
	sub	sp, sp, #28
	str	r0, [fp, #-44]
	str	r1, [fp, #-40]
	mov	r3, #0
	mov	r4, #0
	str	r3, [fp, #-36]
	str	r4, [fp, #-32]
	bl	MessungDerMasse
	mov	r3, #0
	str	r3, [fp, #-28]
	b	.L49
.L50:
	bl	MessungDerMasse
	mov	r2, r1
	mov	r1, r0
	ldr	r3, .L52
	ldmia	r3, {r3-r4}
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4
	bl	__subdf3
	mov	r3, r0
	mov	r4, r1
	mov	r1, r3
	mov	r2, r4
	sub	r4, fp, #36
	ldmia	r4, {r3-r4}
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4
	bl	__adddf3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-36]
	str	r4, [fp, #-32]
	ldr	r3, [fp, #-28]
	add	r3, r3, #1
	str	r3, [fp, #-28]
.L49:
	ldr	r3, [fp, #-28]
	cmp	r3, #129
	ble	.L50
	sub	r6, fp, #36
	ldmia	r6, {r5-r6}
	ldr	r0, [fp, #-28]
	bl	__floatsidf
	mov	r3, r0
	mov	r4, r1
	mov	r0, r5
	mov	r1, r6
	mov	r2, r3
	mov	r3, r4
	bl	__divdf3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-36]
	str	r4, [fp, #-32]
	sub	r4, fp, #36
	ldmia	r4, {r3-r4}
	mov	r0, r3
	mov	r1, r4
	sub	r3, fp, #44
	ldmia	r3, {r2-r3}
	bl	__divdf3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-24]
	str	r4, [fp, #-20]
	sub	r1, fp, #24
	ldmia	r1, {r0-r1}
	bl	Rundung
	mov	r3, r0
	mov	r0, r3
	sub	sp, fp, #16
	ldmfd	sp!, {r4, r5, r6, fp, pc}
.L53:
	.align	2
.L52:
	.word	masseBecher
	.size	berechnungAnzahlMuenzen, .-berechnungAnzahlMuenzen
	.align	2
	.global	binAusgabeMuenzen
	.type	binAusgabeMuenzen, %function
binAusgabeMuenzen:
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #44
	str	r0, [fp, #-44]
	mov	r3, #-2147483648
	mov	r3, r3, asr #15
	str	r3, [fp, #-8]
	sub	r3, fp, #40
	mov	r2, #0
	str	r2, [r3, #0]
	add	r3, r3, #4
	mov	r2, #0
	str	r2, [r3, #0]
	add	r3, r3, #4
	mov	r2, #0
	str	r2, [r3, #0]
	add	r3, r3, #4
	mov	r2, #0
	str	r2, [r3, #0]
	add	r3, r3, #4
	mov	r2, #0
	str	r2, [r3, #0]
	add	r3, r3, #4
	mov	r2, #0
	str	r2, [r3, #0]
	add	r3, r3, #4
	mov	r2, #0
	str	r2, [r3, #0]
	add	r3, r3, #4
	mov	r2, #0
	str	r2, [r3, #0]
	add	r3, r3, #4
	mov	r3, #0
	str	r3, [fp, #-4]
	b	.L55
.L56:
	ldr	r1, [fp, #-4]
	ldr	r2, [fp, #-44]
	mov	r3, r2, asr #31
	mov	r3, r3, lsr #31
	add	r2, r2, r3
	and	r2, r2, #1
	rsb	r3, r3, r2
	mov	r2, r3
	mvn	r3, #39
	mov	r1, r1, asl #2
	add	r1, fp, r1
	add	r3, r1, r3
	str	r2, [r3, #0]
	ldr	r3, [fp, #-44]
	mov	r2, r3, lsr #31
	add	r3, r2, r3
	mov	r3, r3, asr #1
	str	r3, [fp, #-44]
	ldr	r3, [fp, #-4]
	add	r3, r3, #1
	str	r3, [fp, #-4]
.L55:
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	bgt	.L56
	ldr	r3, [fp, #-40]
	cmp	r3, #1
	bne	.L57
	ldr	r3, [fp, #-8]
	mov	r2, #32768
	str	r2, [r3, #52]
	b	.L58
.L57:
	ldr	r3, [fp, #-8]
	mov	r2, #32768
	str	r2, [r3, #48]
.L58:
	ldr	r3, [fp, #-36]
	cmp	r3, #1
	bne	.L59
	ldr	r3, [fp, #-8]
	mov	r2, #16384
	str	r2, [r3, #52]
	b	.L60
.L59:
	ldr	r3, [fp, #-8]
	mov	r2, #16384
	str	r2, [r3, #48]
.L60:
	ldr	r3, [fp, #-32]
	cmp	r3, #1
	bne	.L61
	ldr	r3, [fp, #-8]
	mov	r2, #8192
	str	r2, [r3, #52]
	b	.L62
.L61:
	ldr	r3, [fp, #-8]
	mov	r2, #8192
	str	r2, [r3, #48]
.L62:
	ldr	r3, [fp, #-28]
	cmp	r3, #1
	bne	.L63
	ldr	r3, [fp, #-8]
	mov	r2, #4096
	str	r2, [r3, #52]
	b	.L64
.L63:
	ldr	r3, [fp, #-8]
	mov	r2, #4096
	str	r2, [r3, #48]
.L64:
	ldr	r3, [fp, #-24]
	cmp	r3, #1
	bne	.L65
	ldr	r3, [fp, #-8]
	mov	r2, #2048
	str	r2, [r3, #52]
	b	.L66
.L65:
	ldr	r3, [fp, #-8]
	mov	r2, #2048
	str	r2, [r3, #48]
.L66:
	ldr	r3, [fp, #-20]
	cmp	r3, #1
	bne	.L67
	ldr	r3, [fp, #-8]
	mov	r2, #1024
	str	r2, [r3, #52]
	b	.L68
.L67:
	ldr	r3, [fp, #-8]
	mov	r2, #1024
	str	r2, [r3, #48]
.L68:
	ldr	r3, [fp, #-16]
	cmp	r3, #1
	bne	.L69
	ldr	r3, [fp, #-8]
	mov	r2, #512
	str	r2, [r3, #52]
	b	.L70
.L69:
	ldr	r3, [fp, #-8]
	mov	r2, #512
	str	r2, [r3, #48]
.L70:
	ldr	r3, [fp, #-12]
	cmp	r3, #1
	bne	.L71
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #52]
	b	.L73
.L71:
	ldr	r3, [fp, #-8]
	mov	r2, #256
	str	r2, [r3, #48]
.L73:
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
	.size	binAusgabeMuenzen, .-binAusgabeMuenzen
	.align	2
	.global	getMasseBecher
	.type	getMasseBecher, %function
getMasseBecher:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
	add	fp, sp, #8
	ldr	r3, .L76
	ldmia	r3, {r3-r4}
	mov	r0, r3
	mov	r1, r4
	bl	Rundung
	mov	r3, r0
	mov	r0, r3
	ldmfd	sp!, {r4, fp, pc}
.L77:
	.align	2
.L76:
	.word	masseBecher
	.size	getMasseBecher, .-getMasseBecher
	.align	2
	.global	getBrutto
	.type	getBrutto, %function
getBrutto:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
	add	fp, sp, #8
	sub	sp, sp, #8
	ldr	r3, .L80
	ldmia	r3, {r1-r2}
	ldr	r3, .L80+4
	ldmia	r3, {r3-r4}
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4
	bl	__adddf3
	mov	r3, r0
	mov	r4, r1
	str	r3, [fp, #-16]
	str	r4, [fp, #-12]
	sub	r1, fp, #16
	ldmia	r1, {r0-r1}
	bl	Rundung
	mov	r3, r0
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L81:
	.align	2
.L80:
	.word	masseBecher
	.word	MasseOhneBecher
	.size	getBrutto, .-getBrutto
	.global	__fixdfsi
	.align	2
	.global	Rundung
	.type	Rundung, %function
Rundung:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
	add	fp, sp, #8
	sub	sp, sp, #12
	str	r0, [fp, #-20]
	str	r1, [fp, #-16]
	mov	r3, #0
	str	r3, [fp, #-12]
	sub	r1, fp, #20
	ldmia	r1, {r0-r1}
	mov	r2, #1069547520
	add	r2, r2, #2097152
	mov	r3, #0
	bl	__adddf3
	mov	r3, r0
	mov	r4, r1
	mov	r0, r3
	mov	r1, r4
	bl	__fixdfsi
	mov	r3, r0
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-12]
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
	.size	Rundung, .-Rundung
	.ident	"GCC: (GNU) 4.4.1"
