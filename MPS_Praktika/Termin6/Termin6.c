#include "../h/pio.h"
#include "stdbool.h"
#include "../h/aic.h"

const float muenzGewicht = 5.74;

void taste_irq_handler (void) __attribute__ ((interrupt));

int Tara, Netto, Brutto, AnzahlMuenzen;
bool wait = true;

int main(void)
{	
    inits();
	PIO_Init();	
	Timer_Init();
    
    StructPIO* piobaseB = PIOB_BASE;
    piobaseB->PIO_PER = KEY1 | KEY2 | ALL_LEDS; // Taste 1, 2 & 3 aktivieren
    
        // Interruptanweisung:
    StructAIC* aicbase = AIC_BASE; // Basisadresse AIC
    
        //Interruptanweisungen:
    aicbase->AIC_IDCR = 0x4000; // Interrupt PIOB ausschalten (1<<14)
    aicbase->AIC_ICCR = 0x4000; // Interrupt PIOB löschen
    
    aicbase->AIC_SVR[PIOB_ID] = (unsigned int)taste_irq_handler; // SourceVectorRegister angeben->interrupt Routine
    //aicbase->AIC_SMR[PIOB_ID] = 0x7; // SourceModerRegister Priorität angeben
    //aicbase->AIC_IECR = 0x4000; // Interrupt PIOB einschalten
    
 
    
while(true){
	
        puts ("\n");
        puts ("Hallo!\n");
        puts ("Um das Gefaess zu tarieren, stellen Sie ein leeres Gefaess auf die Waage\n");
        puts ("und druecken Sie die Taste SW1.\n");
        

    
        wait = true;
        while(wait)
        {
            if (!(piobaseB->PIO_PDSR & KEY1))
            {
                puts ("Gefaessgewicht: ");
                convertInt(wiegeBecher());
                puts ("\n");
                wait = false;
            }
        }
        
        puts ("\n");
        puts ("Legen Sie jetzt die Muenzen in den Behaelter.\n");
        puts ("Der Wiegevorgang beginnt, sobald Sie die Taste SW2 druecken.\n");
        
        ///
        
        wait=true;
        
        while(wait)
        {
            if(!(piobaseB->PIO_PDSR & KEY2))
            {
                aicbase->AIC_ICCR = 0x4000; // Interrupt PIOB löschen
                aicbase->AIC_IECR = 0x4000; // Interrupt PIOB einschalten
                // Interrupt durch KEY3 erlauben:
                piobaseB->PIO_IER = KEY3;
                piobaseB->PIO_PER = KEY1 | KEY2 | KEY3 | ALL_LEDS; // Taste 1, 2 & 3 aktivieren
                wait=false;
            }
        }
        
        wait=true;
        
        while(wait){
            int tmpNetto = 0;
            int Anzahl = 0;
            
            Anzahl = berechnungAnzahlMuenzen(muenzGewicht);
            binAusgabeMuenzen(Anzahl);
            tmpNetto=wiegeMuenzen();
            convertInt(tmpNetto);
            puts ("\n");
            
            //if(!(piobaseB->PIO_PDSR & KEY3))
            //{
                Tara= getMasseBecher();
                Netto= tmpNetto;
                Brutto= getBrutto();
                AnzahlMuenzen= Anzahl;
                
            //}
            
            
        }
        
        puts ("\n");
        
        puts ("Tara: ");
        convertInt(Tara);
        puts ("\n");
        
        puts ("Netto: ");
        convertInt(Netto);
        puts ("\n");
        
        puts ("Brutto: ");
        convertInt(Brutto);
        puts ("\n");
        
        puts ("Anzahl Muenzen: ");
        convertInt(AnzahlMuenzen);
        puts ("\n");
        
        puts ("\n");
        
        piobaseB->PIO_SODR = ALL_LEDS;  // Ausgabe Register setzen (AUS)
    
}

        aicbase->AIC_IDCR = 1 << 14; // Interrupt PIOB ausschalten
        aicbase->AIC_ICCR = 1 << 14; // Interrupt PIOB löschen

	return 0;
} 



void taste_irq_handler (void)
{
    StructPIO* piobaseB = PIOB_BASE; // Basisadresse PIO B
    StructAIC* aicbase = AIC_BASE; // Basisadresse AIC
    
    // Inhalt der Interrupt Service Routine
    if ((piobaseB->PIO_PDSR & KEY3)) {  
        wait=false;
    }

    
    // InterruptStatusRegister lesen und in End of Interrupt Command Register (EOICR) schreiben
    aicbase->AIC_EOICR = piobaseB->PIO_ISR; // WICHTIG um Routine zu beenden!
}
