	.file	"Termin6.c"
	.global	muenzGewicht
	.section	.rodata
	.align	2
	.type	muenzGewicht, %object
	.size	muenzGewicht, 4
muenzGewicht:
	.word	1085779476
	.comm	Tara,4,4
	.comm	Netto,4,4
	.comm	Brutto,4,4
	.comm	AnzahlMuenzen,4,4
	.global	wait
	.data
	.type	wait, %object
	.size	wait, 1
wait:
	.byte	1
	.section	.rodata
	.align	2
.LC0:
	.ascii	"\012\000"
	.align	2
.LC1:
	.ascii	"Hallo!\012\000"
	.align	2
.LC2:
	.ascii	"Um das Gefaess zu tarieren, stellen Sie ein leeres "
	.ascii	"Gefaess auf die Waage\012\000"
	.align	2
.LC3:
	.ascii	"und druecken Sie die Taste SW1.\012\000"
	.align	2
.LC4:
	.ascii	"Gefaessgewicht: \000"
	.align	2
.LC5:
	.ascii	"Legen Sie jetzt die Muenzen in den Behaelter.\012\000"
	.align	2
.LC6:
	.ascii	"Der Wiegevorgang beginnt, sobald Sie die Taste SW2 "
	.ascii	"druecken.\012\000"
	.global	__extendsfdf2
	.align	2
.LC7:
	.ascii	"Tara: \000"
	.align	2
.LC8:
	.ascii	"Netto: \000"
	.align	2
.LC9:
	.ascii	"Brutto: \000"
	.align	2
.LC10:
	.ascii	"Anzahl Muenzen: \000"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
	add	fp, sp, #8
	sub	sp, sp, #16
	bl	inits
	bl	PIO_Init
	bl	Timer_Init
	mov	r3, #-2147483648
	mov	r3, r3, asr #15
	str	r3, [fp, #-24]
	ldr	r2, [fp, #-24]
	mov	r3, #65280
	add	r3, r3, #24
	str	r3, [r2, #0]
	mov	r3, #-2147483648
	mov	r3, r3, asr #19
	str	r3, [fp, #-20]
	ldr	r3, [fp, #-20]
	mov	r2, #16384
	str	r2, [r3, #292]
	ldr	r3, [fp, #-20]
	mov	r2, #16384
	str	r2, [r3, #296]
	ldr	r2, .L10
	ldr	r3, [fp, #-20]
	str	r2, [r3, #184]
.L8:
	ldr	r0, .L10+4
	bl	puts
	ldr	r0, .L10+8
	bl	puts
	ldr	r0, .L10+12
	bl	puts
	ldr	r0, .L10+16
	bl	puts
	ldr	r3, .L10+20
	mov	r2, #1
	strb	r2, [r3, #0]
	b	.L2
.L3:
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #60]
	and	r3, r3, #8
	cmp	r3, #0
	bne	.L2
	ldr	r0, .L10+24
	bl	puts
	bl	wiegeBecher
	mov	r3, r0
	mov	r0, r3
	bl	convertInt
	ldr	r0, .L10+4
	bl	puts
	ldr	r3, .L10+20
	mov	r2, #0
	strb	r2, [r3, #0]
.L2:
	ldr	r3, .L10+20
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L3
	ldr	r0, .L10+4
	bl	puts
	ldr	r0, .L10+28
	bl	puts
	ldr	r0, .L10+32
	bl	puts
	ldr	r3, .L10+20
	mov	r2, #1
	strb	r2, [r3, #0]
	b	.L4
.L5:
	ldr	r3, [fp, #-24]
	ldr	r3, [r3, #60]
	and	r3, r3, #16
	cmp	r3, #0
	bne	.L4
	ldr	r3, [fp, #-20]
	mov	r2, #16384
	str	r2, [r3, #296]
	ldr	r3, [fp, #-20]
	mov	r2, #16384
	str	r2, [r3, #288]
	ldr	r3, [fp, #-24]
	mov	r2, #32
	str	r2, [r3, #64]
	ldr	r2, [fp, #-24]
	mov	r3, #65280
	add	r3, r3, #56
	str	r3, [r2, #0]
	ldr	r3, .L10+20
	mov	r2, #0
	strb	r2, [r3, #0]
.L4:
	ldr	r3, .L10+20
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L5
	ldr	r3, .L10+20
	mov	r2, #1
	strb	r2, [r3, #0]
	b	.L6
.L7:
	mov	r3, #0
	str	r3, [fp, #-16]
	mov	r3, #0
	str	r3, [fp, #-12]
	ldr	r3, .L10+36
	ldr	r3, [r3, #0]	@ float
	mov	r0, r3
	bl	__extendsfdf2
	mov	r3, r0
	mov	r4, r1
	mov	r0, r3
	mov	r1, r4
	bl	berechnungAnzahlMuenzen
	mov	r3, r0
	str	r3, [fp, #-12]
	ldr	r0, [fp, #-12]
	bl	binAusgabeMuenzen
	bl	wiegeMuenzen
	mov	r3, r0
	str	r3, [fp, #-16]
	ldr	r0, [fp, #-16]
	bl	convertInt
	ldr	r0, .L10+4
	bl	puts
	bl	getMasseBecher
	mov	r2, r0
	ldr	r3, .L10+40
	str	r2, [r3, #0]
	ldr	r3, .L10+44
	ldr	r2, [fp, #-16]
	str	r2, [r3, #0]
	bl	getBrutto
	mov	r2, r0
	ldr	r3, .L10+48
	str	r2, [r3, #0]
	ldr	r3, .L10+52
	ldr	r2, [fp, #-12]
	str	r2, [r3, #0]
.L6:
	ldr	r3, .L10+20
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L7
	ldr	r0, .L10+4
	bl	puts
	ldr	r0, .L10+56
	bl	puts
	ldr	r3, .L10+40
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	convertInt
	ldr	r0, .L10+4
	bl	puts
	ldr	r0, .L10+60
	bl	puts
	ldr	r3, .L10+44
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	convertInt
	ldr	r0, .L10+4
	bl	puts
	ldr	r0, .L10+64
	bl	puts
	ldr	r3, .L10+48
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	convertInt
	ldr	r0, .L10+4
	bl	puts
	ldr	r0, .L10+68
	bl	puts
	ldr	r3, .L10+52
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	convertInt
	ldr	r0, .L10+4
	bl	puts
	ldr	r0, .L10+4
	bl	puts
	ldr	r3, [fp, #-24]
	mov	r2, #65280
	str	r2, [r3, #48]
	b	.L8
.L11:
	.align	2
.L10:
	.word	taste_irq_handler
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	wait
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	muenzGewicht
	.word	Tara
	.word	Netto
	.word	Brutto
	.word	AnzahlMuenzen
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.size	main, .-main
	.align	2
	.global	taste_irq_handler
	.type	taste_irq_handler, %function
taste_irq_handler:
	@ Interrupt Service Routine.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r2, r3, fp}
	add	fp, sp, #8
	sub	sp, sp, #8
	mov	r3, #-2147483648
	mov	r3, r3, asr #15
	str	r3, [fp, #-16]
	mov	r3, #-2147483648
	mov	r3, r3, asr #19
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-16]
	ldr	r3, [r3, #60]
	and	r3, r3, #32
	cmp	r3, #0
	beq	.L13
	ldr	r3, .L15
	mov	r2, #0
	strb	r2, [r3, #0]
.L13:
	ldr	r3, [fp, #-16]
	ldr	r2, [r3, #76]
	ldr	r3, [fp, #-12]
	str	r2, [r3, #304]
	sub	sp, fp, #8
	ldmfd	sp!, {r2, r3, fp}
	subs	pc, lr, #4
.L16:
	.align	2
.L15:
	.word	wait
	.size	taste_irq_handler, .-taste_irq_handler
	.ident	"GCC: (GNU) 4.4.1"
